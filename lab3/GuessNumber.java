import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber1 {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); 
		Random rand = new Random(); 
		int number =rand.nextInt(100);  
        
        int guess;
        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int attempt = 0;

         do{
            
            guess = reader.nextInt();
            attempt = ++attempt;           
            if (guess == -1)
                System.out.println("Sorry,the number was " + number);               
            else if (guess == number){
                System.out.print("Congrats."); 
                System.out.println("You won after "+ attempt +" attempts!");}               
            else{
                System.out.println("Sorry!");
                if (guess < number)
                    System.out.println("Mine is greater!");
                else
                    System.out.println("Mine is smaller!");
                
                System.out.println("type -1 to quit or give me a guess: ");}
          }  
                
          while(number != guess && guess != -1);
            
            
                      
		reader.close(); 
	}
	
	
}

