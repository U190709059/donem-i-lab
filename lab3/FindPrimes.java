public class FindPrimes {

	public static void isPrime(int x){
		int divider = 2;
		boolean flag = true;
		while (divider<x && flag==true){
			if (x%divider==0)
				flag=!flag;
			divider++;}
		if (flag==true)
			System.out.print(x+" ");
        }

	public static void main(String[] args){
		int a = Integer.parseInt(args[0]);
		if (a==1 || a==0)
			System.out.print(a+" is not a prime number!");
		else if (a<0)
			System.out.print("There is no negative prime number!");
		else{		
			for(int i=2;i<=a;i++)
				isPrime(i);}
	}
}		



